import React, {ReactElement} from "react";
import './App.scss'
import ResultLeft from "./components/results/ResultLeft.tsx";
import ResultRight from "./components/results/ResultRight.tsx";

const App: React.FC = ():ReactElement =>  {

  return (
      <div className="result">
        <ResultLeft/>
        <ResultRight/>
      </div>
  )
}

export default App
