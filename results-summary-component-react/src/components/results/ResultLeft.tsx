import React, {ReactElement} from "react";
import './ResultLeft.scss'

const ResultLeft: React.FC = ():ReactElement => {
    return (
        <div className="result__left">
            <p className="result__title result__title-gray">Your Result</p>
            <div className="result__score">
                <p className="result__score-actual">76</p>
                <p className="result__score-total">of 100</p>
            </div>
            <div className="result__description">
                <p className="result__description-title">Great</p>
                <p className="result__description-content">You scored higher than 65% of the people who have taken these
                    tests</p>
            </div>
        </div>
    )
}

export default ResultLeft;