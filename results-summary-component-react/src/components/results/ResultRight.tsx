import React from "react";
import './ResultRight.scss'

const ResultRight: React.FC = () => {
    return (
        <div className="result__right">
            <div className="result__list">
                <p className="result__title">Summary</p>
                <div className="result__item result__item-reaction">
                    <p className="result__item-title">Reaction</p>
                    <p className="result__item-score">80 <span>/ 100</span></p>
                </div>
                <div className="result__item result__item-memory">
                    <p className="result__item-title">Memory</p>
                    <p className="result__item-score">92 <span>/ 100</span></p>
                </div>
                <div className="result__item result__item-verbal">
                    <p className="result__item-title">Verbal</p>
                    <p className="result__item-score">61 <span>/ 100</span></p>
                </div>
                <div className="result__item result__item-visual">
                    <p className="result__item-title">Visual</p>
                    <p className="result__item-score">72 <span>/ 100</span></p>
                </div>
            </div>
            <button type="button" className="result__button">Continue</button>
        </div>
    )
}

export default ResultRight;